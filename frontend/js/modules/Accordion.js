"use strict";

import './optimizedResize'

export default class Accordion {

	constructor(selector, options = {}) {
		let self = this;
		// get parent selector
		this.parent = document.querySelector(selector);
		if (!this.parent) return;
		// find all accordions with data-js=="accordion"
		this.accordions = this.parent.querySelectorAll('[data-js="accordion"]');
		if (this.accordions.length < 1) return;

		this.defaultOption = {
			activeClass: "is-open",
			speed: 300,
			onInit: function () {
			},
			onOpen: function () {
			},
			onClose: function () {
			}
		};


		// extend options
		this.settings = Object.assign(this.defaultOption, options);

		// init accordions
		try {
			Array.prototype.forEach.call(this.accordions, (el) => this.init.call(self, el));
		} catch (e) {
			console.error("Accordion: something went wrong with \n" + e);
		}

		return this.accordions;
	}

	init(el) {
		let _self = this;
		// check if accordion was already initialize
		let data = el.getAttribute('data-accordion');
		if (data) return;
		let btn = el.firstElementChild;
		btn.addEventListener("click", function () {
			if (!(el.classList.contains(_self.settings.activeClass))) {
				_self.closeAll();
				_self.open(el);
			} else {
				_self.close(el);
			}
		});

		el.setAttribute('data-accordion', true);

		// check location hash
		if (window.location.href.split('#')[1] == el.id) {
			_self.open(el);
		}

		if (typeof _self.settings.onInit == 'function') _self.settings.onInit.call(el);

	}

	open(el) {
		let _self = this;
		el.classList.add(this.settings.activeClass);
		let panel = el.lastElementChild;
		panel.style.maxHeight = panel.scrollHeight + "px";

		// handle resize event
		window.addEventListener("optimizedResize", function() {
			if (panel.style.maxHeight) {
				panel.style.maxHeight = panel.scrollHeight + "px";
			}
		});

		if (typeof _self.settings.onOpen == 'function') _self.settings.onOpen.call(el);
	}

	close(el) {
		let _self = this;
		el.classList.remove(this.settings.activeClass);
		let panel = el.lastElementChild;
		panel.style.maxHeight = null;
		if (typeof _self.settings.onClose == 'function') _self.settings.onClose.call(el);
	}

	closeAll() {
		let _self = this;
		Array.prototype.forEach.call(this.accordions, (el) => this.close.call(_self, el));
	}
}

