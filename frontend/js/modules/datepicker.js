$(function () {

	var checkInInput = document.getElementById("check-in-date");
	var checkOutInput = document.getElementById("check-out-date");

	var datepickerBoxLabel = document.querySelectorAll(".js-pmu-label");
	var checkInDayLabel = document.getElementById("check-in-day");
	var checkOutDayLabel = document.getElementById("check-out-day");
	var checkInMonthLabel = document.getElementById("check-in-month");
	var checkOutMonthLabel = document.getElementById("check-out-month");

	var datepickerCheckIn = document.getElementById("js-pmu-check-in");
	var datepickerCheckOut = document.getElementById("js-pmu-check-out");

	var checkInInstance;
	var checkOutInstance;

	function getMonthName(month) {
		var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		return months[month - 1];
	}

	function plusOneDay(date) {
		date.setDate(date.getDate() + 1);
		var nextDay = new Date(date);
		return nextDay;
	}

	function setDate() {
		var checkInDate = checkInInstance.get_date(false);
		var checkOutDate = checkOutInstance.get_date(false);

		if (+checkInDate >= +checkOutDate) {
			// get Date object from checkIn datepicker and plus one day
			checkOutDate = plusOneDay(checkInInstance.get_date(false));
			checkOutInstance.set_date(checkOutDate);
		}

		var checkInDay = checkInDate.getDate();
		var checkInMonth = checkInDate.getMonth() + 1; //January is 0!;
		var checkInYear = checkInDate.getFullYear();
		var checkOutDay = checkOutDate.getDate();
		var checkOutMonth = checkOutDate.getMonth() + 1; //January is 0!;
		var checkOutYear = checkOutDate.getFullYear();
		var checkInMonthName = getMonthName(checkInMonth);
		var checkOutMonthName = getMonthName(checkOutMonth);

		// set input value
		checkInDate = [checkInDay, checkInMonth, checkInYear].join("-");
		checkOutDate = [checkOutDay, checkOutMonth, checkOutYear].join("-");
		checkInInput.value = checkInDate;
		checkOutInput.value = checkOutDate;

		// set label value
		checkInDayLabel.innerHTML = checkInDay;
		checkInMonthLabel.innerHTML = checkInMonthName;
		checkOutDayLabel.innerHTML = checkOutDay;
		checkOutMonthLabel.innerHTML = checkOutMonthName;

		//console.log(checkInDate, checkOutDate);
	}

	// init datepicker
	function initDatepicker(datepicker, minDate, checkOutDate) {

		if (!minDate) {
			// not including today
			minDate = minDate || new Date();
			minDate.setDate(minDate.getDate() - 1);
		}

		pickmeup(datepicker, {
			flat: true,
			mode: 'single',
			calendars: 1,
			format: "d-m-Y",
			render: function (date) {
				if (date < minDate) {
					return {
						disabled: true,
						class_name: 'date-in-past'
					};
				}
				if (checkOutDate) {
					if (date >= minDate && date <= checkOutDate) {
						console.log(date.getDate() + ": " + "is selected");
						return {
							class_name: 'pmu-selected'
						};
					}
				}
				return {};
			}
		});
		if (checkOutDate) {
			pickmeup(datepicker).set_date(checkOutDate);
		}
		return pickmeup(datepicker);
	}

	function destroyDatepicker(object, html) {
		// native destroy mathod not work :-(
		object = null;
		html.innerHTML = "";
		html.__pickmeup = null;
	}
	try {
		// init datepicker
		checkInInstance = initDatepicker(datepickerCheckIn);
		checkOutInstance = initDatepicker(datepickerCheckOut, new Date());
		setDate();

		// events listeners
		datepickerCheckIn.addEventListener('pickmeup-change', function (e) {
			var InDate = checkInInstance.get_date(false);
			destroyDatepicker(checkOutInstance, datepickerCheckOut);
			initDatepicker(datepickerCheckOut, plusOneDay(InDate));
			setDate();
		});

		datepickerCheckOut.addEventListener('pickmeup-change', function (e) {
			var InDate = checkInInstance.get_date(false);
			var OutDate = checkOutInstance.get_date(false);

			// reinit datepicker
			destroyDatepicker(checkOutInstance, datepickerCheckOut);
			checkOutInstance = initDatepicker(datepickerCheckOut, InDate, OutDate);
			// initDatepicker is 
			setTimeout(setDate, 0);
		});

	} catch (e) {
		console.log(e);
	}

}); //ready