//Animate CSS + WayPoints javaScript Plugin
//Example: $(".element").animated("zoomInUp", "zoomOutDown");
'use strict';

import {detect} from './detect'

(function ($) {
	$.fn.waypointTriger = function (cb) {
		$(this).waypoint(function (dir) {
			if (dir === "down") {
				cb($(this));
			}
		}, {
			offset: $(window).height()
		}).waypoint(function (dir) {
			if (dir === "up") {
				cb($(this));
			}
		}, {
			offset: -$(window).height()
		});
	};
})(jQuery);

jQuery(function ($) {
	function forwardAnimation(startClass, endClass) {
		$(this).removeClass(endClass).addClass(startClass);
		// console.log("forward class: " + startClass);
	}

	function backwardAnimation(startClass, endClass) {
		$(this).removeClass(startClass).addClass(endClass);
		// console.log("backward class: " + endClass);
	}

	if (!detect.isMobile()) {
		$(window).on("load", function () {
			Waypoint.refreshAll();
		});

		$("[data-scroll-animation]").each(function () {

			$(this).addClass('animated');
			$(this).css('opacity', 0);

			var classes = $(this).data('scroll-animation').split(',');

			if (classes.length === 2) {
				// TOP edge of element equal BOTTOM of the viewport
				$(this).waypoint(function (direction) {
					$(this.element).css('opacity', 1);
					if (direction === "down") {
						forwardAnimation.call(this.element, classes[0], classes[1]);
					} else {
						backwardAnimation.call(this.element, classes[0], classes[1]);
					}
				}, {
					offset: function () {
						return $(window).height()
					}
				});

				// BOTTOM edge of element equal TOP of the viewport
				$(this).waypoint(function (direction) {
					$(this.element).css('opacity', 1);
					if (direction === "down") {
						backwardAnimation.call(this.element, classes[0], classes[1]);
					} else {
						forwardAnimation.call(this.element, classes[0], classes[1]);
					}
				}, {
					offset: function () {
						return -this.element.clientHeight;
					}
				});

			} else if (classes.length === 1) {

				// TOP of element hit the BOTTOM of viewport
				$(this).waypoint(function (direction) {
					$(this.element).css('opacity', 1);
					if (direction === "down") {
						forwardAnimation.call(this.element, classes[0], classes[1]);
					}
				}, {
					offset: function () {
						return $(window).height()
					}
				});

				// BOTTOM of element hit the TOP of viewport
				$(this).waypoint(function (direction) {
					$(this.element).css('opacity', 1);
					if (direction === "up") {
						forwardAnimation.call(this.element, classes[0], classes[1]);
					}
				}, {
					offset: function () {
						return -this.element.clientHeight;
					}
				});
			} else {
				console.error("Data scroll animation error: " + classes);
				return;
			}
		});
	}
}); //ready