(function ($) {
	// prived methods
	var createBtns = function () {
		var $plusBtn = $('<i class="fa fa-plus q-inpt-btn plus"></i>');
		var $minusBtn = $('<i class="fa fa-minus q-inpt-btn minus"></i>');
		$(this).append($plusBtn);
		$(this).prepend($minusBtn);
	};

	var increment = function (oldValue, settings) {
		var newVal;
		if (settings.maxValue !== false) {
			if (oldValue < settings.maxValue) {
				newVal = oldValue + settings.iterValue;
			} else {
				newVal = settings.maxValue;
			}
		} else {
			newVal = oldValue + settings.iterValue;
		}
		return (newVal);
	};

	var decrement = function (oldValue, settings) {
		var newVal;
		if (settings.minValue !== false) {
			if (oldValue > settings.minValue) {
				newVal = oldValue - settings.iterValue;
			} else {
				newVal = settings.minValue;
			}
		} else {
			newVal = oldValue - settings.iterValue;
		}
		return (newVal);
	};

	var initBtn = function (settings) {
		var $input = $(this).find("input");
		//console.log(settings);

		$(this).find(".q-inpt-btn").on("click", function () {
			var $button = $(this);
			var oldValue = parseFloat($input.val()) || settings.initValue || 0;
			var newVal;
			if ($button.hasClass("plus")) {
				newVal = increment(oldValue, settings);
			} else if ($button.hasClass("minus")) {
				newVal = decrement(oldValue, settings);
			}
			$input.val(newVal).trigger("change");
			
		});
	};
	// prived methods end
	var methods = {
		init: function (options) {

			// default option
			var settings = $.extend({
				iterValue: 1, // number
				initValue: 1, // number
				minValue: 1, // number or false
				maxValue: false, // number or false
				init: function() {},
				change: function() {}
			}, options);



			//console.log("init fn");

			return this.each(function () {
				var $this = $(this),
					init = "init",
					data = $this.data("numberInput");

				// Если плагин ещё не проинициализирован
				if (!data) {
					createBtns.apply(this, [settings]);
					initBtn.apply(this, [settings]);
					$(this).data("numberInput", init);
					if ( settings.init ) {
						// init callback
						settings.init(this);
					}
					/* 
					if ( settings.change ) {
						$(this).find("input").on("change", settings.change);
					} 
					*/
				}
			});
		},
		set: function (value) {
			return this.each(function () {
				var $this = $(this),
					input = $this.find("input");
				input.val(value);
			});
		},

		get: function () {
			var value = [];
			this.each(function () {
				var $this = $(this),
					input = $this.find("input");
				value.push(+input.val());
			});
			return value;
		}

	};

	$.fn.numberInput = function (method) {

		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('method ' + method + ' not exist for jQuery.numberInput');
		}

	};

})(jQuery);


$(function () {
	// init
	/*
	$(selector).numberInput({
		iterValue: 1,
		initValue: 1,
		minValue: false,
		maxValue: false,
		init: function() {}
	});
	*/
}); // ready
