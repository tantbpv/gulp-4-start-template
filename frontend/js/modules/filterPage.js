/*
* DO NOT USE!
*
* REFACTOR NEEDED
*
* */


var filterPageName = (function ($) {

	if (typeof projectAjaxData == 'undefined') return;

	var _ajaxData = projectAjaxData;
	var _filtersForm = new FilterForm("#portfolio-filter");
	var cellClass = 'portfolio_cell';
	var $grid = $("#portfolio-grid");

	// private methods

	var _filtersFormSubmitHandler = function () {
		// disable form while ajax
		_filtersForm.disableForm();

		setTimeout(function () {

			//set url query string
			_filtersForm.setUrl(function () {

				//add filters parameters to _ajaxData

				var _formData = _filtersForm.serializeForm();

				_ajaxData.filters = UrlParser.decodeUrl(_formData);

				getItems(function () {
					// enable form after ajax
					_filtersForm.enableForm();
				});
			});
		}, 5); // setTimeout

	};

	var appendItems = function (count, cb) {
		if (!(Array.isArray(_ajaxData.entries) && _ajaxData.entries.length)) return;
		for (var i = 0; i < count; i++) {
			if (_ajaxData.entries.length) {
				$grid.append('<div class="' + cellClass + '">' + _ajaxData.entries.shift() + '</div>');
			}
		}

		// refresh animation
		animateItems();
		Waypoint.refreshAll();
		// console.log("entries: " + _ajaxData.entries.length);
		if (typeof cb === "function" && typeof cb !== "undefined") {
			cb();
		}
	};

	var getItems = function (cb) {
		// clear old entries
		$grid.html("");

		// launch AJAX and get new data
		WP_Ajax.get_json(_ajaxData, function (response) {

			// set global _ajaxData
			_ajaxData.entries = response.entries;

			// add N new entries to page
			appendItems(_ajaxData.increment);

			if (typeof cb === "function" && typeof cb !== "undefined") {
				cb();
			}

		});
	};

	var animateItems = function (cb) {
		var _showClass = "is-shown";
		var _itemClass = "." + cellClass;
		// find not animated items
		var $items = $grid.find(_itemClass + ":not(." + _showClass + ")");
		var viewportBottom = $(window).scrollTop() + $(window).height();

		$items.each(function (i) {
			// Is item in viewport
			if ($(this).offset().top <= viewportBottom) {
				$(this).addClass(_showClass);
			}
		});

		if (typeof cb === "function" && typeof cb !== "undefined") {
			cb();
		}
	};

	function onScroll() {
		var viewportBottom = $(window).scrollTop() + $(window).height();
		var containerBottom = $grid.offset().top + $grid.outerHeight();
		var breakpoint = containerBottom - 1000;

		// End of the document reached?
		var pause = false;
		if (viewportBottom >= breakpoint && !pause) {
			pause = true;
			// append next items
			appendItems(_ajaxData.increment, function () {
				pause = false;
			});

		}
	};

	_filtersForm.onSubmit = _filtersFormSubmitHandler;

	if (!_filtersForm.onLoad()) {
		getItems();
	}

	$(window).on("scroll", animateItems);
	$(window).on("scroll", onScroll);

	return {
		// public methods
	}
}(jQuery));