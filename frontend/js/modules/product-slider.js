var ProductSlider = (function ($) {
	// private methods
	// arrow icons
	var arrowLeft = '<i class="icon-left-arrow" aria-hidden="true"></i>';
	var arrowRight = '<i class="icon-right-arrow" aria-hidden="true"></i>';

	// elevatezoom
	var $image;
	var zoomConfig = {
		zoomType: "inner",
		scrollZoom: true,
		cursor: "zoom-in"
	};

	function isDesctop() {
		return window.matchMedia("(min-width: 62em)").matches;
	}

	function initZoom(img) {
		if (isDesctop()) {
			img.elevateZoom(zoomConfig);
		}
	}

	function destroyZoom(img) {
		$.removeData(img, 'elevateZoom'); //remove zoom instance from image
		$('.zoomContainer').remove(); // remove zoom container from DOM
	}

	return {
		// public methods
		init: function (slider, thumbs) {
			var $slider = $(slider);
			var $thumbs = $(thumbs);

			var sliderOption = {
				slidesToShow: 1,
				slidesToScroll: 1,
				fade: true,
				arrows: true,
				nextArrow: '<button type="button" class="slider_btn--next slider_btn"> ' + arrowRight + ' </button>',
				prevArrow: '<button type="button" class="slider_btn--prev slider_btn">' + arrowLeft + '</button>',
				swipe: false,
				asNavFor: thumbs,
				adaptiveHeight: true,
				//accessibility: false, // prevent page scroll up on autoplay
				responsive: [
					{
						breakpoint: 768,
						settings: {
							swipe: true
						}
					}
				]
			};

			var thumbsOption = {
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: false,
				swipeToSlide: true,
				infinite: true,
				asNavFor: slider,
				focusOnSelect: true
				//accessibility: false // prevent page scroll up on autoplay
			};

			// init slider
			$slider.slick(sliderOption);
			$slider.slick('slickGoTo', 0);
			// init thumbs
			$thumbs.slick(thumbsOption);
			$thumbs.slick('slickGoTo', 0);

			// slider events
			/*	productSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
			 $image = $(slick.$slides[currentSlide]).find("img");
			 destroyZoom($image);
			 });
			 $slider.on('afterChange', function (event, slick, currentSlide) {
			 $image = $(slick.$slides[currentSlide]).find("img");
			 initZoom($image);
			 });
			 $slider.on('init', function (event, slick) {
			 $image = $(slick.$slides[0]).find("img");
			 initZoom($image);
			 });
			 $slider.on("click", function () {
			 $slider.slick("slickNext");
			 });

			 if ($slider.length) {
			 $(window).on("resize", function () {
			 destroyZoom($image);
			 setTimeout(function () {
			 initZoom($image);
			 }, 10);
			 });
			 }*/

			// thumbs events
			$thumbs.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				//remove all active class
				$thumbs.find(".slick-slide").removeClass('slick-is-current');
				//set active class for current slide
				$thumbs.find('.slick-slide[data-slick-index=' + nextSlide + ']').addClass('slick-is-current');
			});

		}
	}
}(jQuery));

jQuery(function ($) {
	ProductSlider.init(".js-product-slider", ".js-product-thumbs");
}); // ready
