/*
* Using bootstrap-select by Silvio Moreto jQuery plugin for style select;
*
* https://github.com/silviomoreto/bootstrap-select
*
* */
import "./bootstrap-drop-down"


export let Select = (function ($) {

	let _this;

	let init = function (selector) {
		_this = $(selector);

		// init
		_this.each(function () {
			$(this).selectpicker({
				size: 4
			});
		});
	};



	return {
		// public methods
		init: init,
		refresh: function () {
			_this.trigger('refresh');
		},
		reset: function (selector) {
			select = selector? $(selector) : _this;
			select.find("option[selected='selected']").each(function () {
				$(this).removeAttr('selected');
			});
			select.find("option:first").attr('selected','selected');
			// refresh select
			setTimeout(function () {
				select.trigger('refresh');
			}, 0);
		}
	}
}(jQuery));

jQuery(function ($) {
	let select = Select.init(".select");
});