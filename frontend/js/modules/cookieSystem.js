export let cookieSystem = {
	get: function (a) {
		return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(a).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
	},
	set: function (a, b, c, d, e, f) {
		if (!a || /^(?:expires|max\-age|path|domain|secure)$/i.test(a)) return !1;
		let g = "";
		let d = d || "/"; // set default path

		if (c) switch (c.constructor) {
			case Number:
				let h = new Date(), i = new Date(h.getTime() + 864e5 * c);
				g = c === 1 / 0 ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; expires=" + i.toGMTString();
				break;

			case String:
				g = "; expires=" + c;
				break;

			case Date:
				g = "; expires=" + c.toGMTString();
		}
		return document.cookie = encodeURIComponent(a) + "=" + encodeURIComponent(b) + g + (e ? "; domain=" + e : "") + (d ? "; path=" + d : "") + (f ? "; secure" : ""),
				!0;
	},
	remove: function (a, b, c) {
		return !(!a || !this.hasItem(a)) && (document.cookie = encodeURIComponent(a) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (c ? "; domain=" + c : "") + (b ? "; path=" + b : ""),
						!0);
	},
	has: function (a) {
		return new RegExp("(?:^|;\\s*)" + encodeURIComponent(a).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=").test(document.cookie);
	}
};
