export let newTab = function (url) {
	let win = window.open(url, '_blank');
	win.focus();
};