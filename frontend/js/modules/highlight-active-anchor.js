jQuery(function ($) {
	var navLinks = $(".js-main-nav a");

	function checkSection() {
		var vScroll = $(window).scrollTop();
		var activeClass = "is-active";
		var currentId, currentHash;

		// remove highlight class
		navLinks.removeClass(activeClass);

		// get current section id
		$("[id]").each(function () {
			var $section = $(this);
			var topEdge = $section.offset().top - 100;
			var bottomEdge = topEdge + $section.height() + 50;

			if (topEdge < vScroll && bottomEdge > vScroll) {
				currentId = $section.attr("id");
			}
		});
		// set active class to link with current id
		navLinks.each(function () {
			var $link = $(this);
			var currentHash = $link.attr('href').replace(/^.*?(#|$)/,'');
			if (currentHash === currentId) {
				$link.addClass(activeClass);
			}
		});
	}

	$(window).on("scroll", function () {
		checkSection();
	});
});//ready