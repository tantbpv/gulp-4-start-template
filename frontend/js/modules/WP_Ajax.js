/*
 Using global object with ajax configs

* var ajaxLoaderData = {
* 	url: [path to admin-ajax.php],
* 	nonce: [nonce code]
* }
* */

export let WP_Ajax = (function ($) {

	if (typeof ajaxLoaderData == 'undefined') return;

	// protected
	var _url = ajaxLoaderData.url;
	var _nonce = ajaxLoaderData.nonce; // wp secure code

	// get data via ajax
	var request = function (data, cb) {
		if (!_url) return;
		data.nonce = _nonce;

		// console.log(ajaxLoaderData.blockAjax);

		if (ajaxLoaderData.blockAjax) return;

		ajaxLoaderData.blockAjax = true;

		$.ajax({
			url: _url,
			type: 'POST',
			data: data,
			success: function (response) {
				// console.log(response);
				if ( response === 0 ) return; // error
				// _response = response;

				ajaxLoaderData.blockAjax = false;

				// fire callback
				if (typeof cb === "function" && typeof cb !== "undefined") {
					cb(response);
				}
			},
			error: function (errorThrown) {
				console.log(errorThrown);
				ajaxLoaderData.blockAjax = false;
				if (typeof cb === "function" && typeof cb !== "undefined") {
					cb(false);
				}
			}
		});
	};

	return {
		// public methods
		request: request
	}
}(jQuery));