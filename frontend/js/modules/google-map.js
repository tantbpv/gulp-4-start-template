
let gMapThemes = {
	ThemeName: []
};


function GoogleMap(id, config) {

	this.id = id;
	this.config = config;
	this.map = null;
	this.marker = null;

	this.initMap = function () {
		var _isDraggable = jQuery(document).width() > 768 ? true : false; // enable drag if document is wider than 768px

		var mapOptions = {
			draggable: _isDraggable,
			center: {
				lat: this.config.mapCenter[0],
				lng: this.config.mapCenter[1]
			},
			// Apply the map style array to the map.
			styles: gMapThemes.ThemeName,
			zoom: this.config.zoom,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			panControl: false,
			zoomControl: true,
			mapTypeControl: false,
			scaleControl: false,
			streetViewControl: true,
			overviewMapControl: false,
			scrollwheel: false // Prevent users to start zooming the map when scrolling down the page
			//... options options options
		};

		// Create a map object and specify the DOM element for display.
		this.map = new google.maps.Map(document.getElementById(this.id), mapOptions);

		if (this.config.marker.center !== undefined) {
			this.addMarker();
		}

		if (this.config.marker.message !== undefined) {
			this.addInfoWindow();
		}

		return this.map;
	};

	this.addMarker = function () {
		// create new marker position instance
		var _markerPosition = new google.maps.LatLng(this.config.marker.center[0], this.config.marker.center[1]);
		// create new marker instance
		this.marker = new google.maps.Marker({
			position: _markerPosition,
			map: this.map,
			title: this.config.marker.title,
			icon: this.config.icon
		});
	};

	this.addInfoWindow = function () {
		// create info window instance
		var _marker = this.marker;
		var _map = this.map;
		var _infoWindow =  new google.maps.InfoWindow({
			content: this.config.marker.message
		});
		_marker.addListener('click', function () {
			_infoWindow.open(_map, _marker);
		});
		// Event that closes the Info Window with a click on the map
		google.maps.event.addListener(_map, 'click', function () {
			_infoWindow.close();
		});
	};
}

GoogleMap.prototype.setCenter = function (latitude, longitude) {
	//centering map
	this.map.panTo({lat: latitude, lng: longitude});
};

// google map api end