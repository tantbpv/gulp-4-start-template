'use strict';

export default class Alert {

	constructor(messages) {
		this.options = {
			showTime: 2000,
			speed: 200,
			activeClass: "is-active"
		};
		this.alertTimeout = null;

		// create Alert element with message
		this.alertElement = document.createElement('section');
		this.alertElement.classList.add('alert');

		if (Array.isArray(messages)) {
			Array.prototype.forEach.call(messages, (message)=> {
				this.alertElement.innerHTML += this.getTemplate(message);
			});
		} else {
			this.alertElement.innerHTML += this.getTemplate(messages);
		}


		// append to DOM
		document.querySelector("body").appendChild(this.alertElement);

		// remove Alert element from DOM
		this.alertTimeout = setTimeout(() => { this.remove(); }, this.options.showTime);

		// add close btn
		// <button class="alert_close">×</button>
		let closeBtn = document.createElement('button');
		closeBtn.classList.add("alert_close");
		closeBtn.textContent = '×';
		closeBtn.addEventListener("click", () => { this.remove(); });
		this.alertElement.appendChild(closeBtn);

		// add hover event listeners
		this.alertElement.addEventListener('mouseover', ()=> {
			clearTimeout(this.alertTimeout);
			this.alertTimeout = null;
		});

		this.alertElement.addEventListener('mouseleave', ()=> {
			this.alertTimeout = setTimeout(() => { this.remove(); }, this.options.showTime);
		});
	}

	getTemplate(message) {
		let template = `
			<div class="alert_item">
				<div class="alert_content">
					<p class="alert_text">${message}</p>
				</div>
			</div>`;
		return template;
	}

	remove() {
		this.alertElement.classList.remove(this.options.activeClass);
		setTimeout(() => {
			clearTimeout(this.alertTimeout);
			this.alertElement.parentNode.removeChild(this.alertElement);
		}, this.options.speed);
	}
}