class XHR {
	get(url) {
		return new Promise(function (resolve, reject) {

			let xhr = new XMLHttpRequest();
			xhr.open('GET', url, true);

			xhr.onload = function () {
				console.log(this);
				if (this.status >= 200 && this.status < 400) {
					resolve(this.response);
				} else {
					let error = new Error(this.statusText);
					error.code = this.status;
					reject(error);
				}
			};

			xhr.onerror = function () {
				reject(new Error("Network Error"));
			};

			xhr.send();
		});
	}

	post(url, data, options = {}) {
		return new Promise(function (resolve, reject) {

			let xhr = new XMLHttpRequest();
			xhr.open('POST', url, true);

			if (options.contentType) {
				xhr.setRequestHeader('Content-Type', options.contentType);
			} //else {
			// 	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
			// }

			xhr.onload = function () {
				console.log(this);
				if (this.status >= 200 && this.status < 400) {
					resolve(this.response);
				} else {
					let error = new Error(this.statusText);
					error.code = this.status;
					reject(error);
				}
			};

			xhr.onerror = function () {
				reject(new Error("Network Error"));
			};

			xhr.send(data);
		});
	}
}

export const xhr = new XHR();

// usage example
/*
 xhr.get("/").then(
 response => console.log(`Fulfilled: ${response}`),
 error => console.log(`Rejected: ${error}`)
 );
 */

/*
 let data = {test: 1};
 xhr.post("/", data).then(
 response => console.log(`Fulfilled: ${response}`),
 error => console.log(`Rejected: ${error}`)
 );
 */
