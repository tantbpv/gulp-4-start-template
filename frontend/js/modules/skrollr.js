'use strict';

import {detect} from './detect'

export let scrollr;

jQuery(function ($) {
	if (!detect.isMobile) {
		setTimeout(function () {
			scrollr = skrollr.init({forceHeight: false});
		}, 10);
	}
});
