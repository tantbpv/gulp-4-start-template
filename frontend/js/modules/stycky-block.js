$(function () {
	var $slideWrapp = $(".js-filter-slide");
	var $slideInner = $slideWrapp.find(".filter-section__inner");
	var slideTrackStart;
	var slideTrackEnd;
	var media;
	var fixMenuHeight = $(".js-fix-menu").height();
	var scrollTop = $(window).scrollTop() + fixMenuHeight;
	var mode = "top"; // sticky / top
	var position = "up"; // middle / down
	function getMode() {
		if (scrollTop < slideTrackStart) {
			position = "up";
			mode = "top";
			return mode;
		} else if (scrollTop >= slideTrackStart && scrollTop < slideTrackEnd && mode === "top") {
			position = "middle";
			mode = "top";
			return mode;
		} else if (scrollTop >= slideTrackStart && scrollTop < slideTrackEnd && mode === "sticky") {
			position = "middle";
			mode = "sticky";
			return mode;
		} else if (scrollTop >= slideTrackEnd) {
			position = "down";
			mode = "sticky";
			return mode;
		} else {
			console.log("getMode() error!!");
		}
	}

	function setSlidePosition() {
		if (position === "up") {
			$slideInner.attr("style", "").removeClass("is-sticky");
		} else if (mode === "top" && position === "middle") {
			$slideInner.removeClass("is-sticky");
		} else if (mode === "sticky" && position === "middle") {
			$slideInner.addClass("is-sticky");
			$slideInner.css("top", fixMenuHeight);
		} else if (position === "down") {
			$slideInner.addClass("is-sticky");
			$slideInner.css("top", fixMenuHeight);
		} else {
			console.log("setSlideWrappPosition() error!!");
		}
	}

	function setWrappHeight() {
		media = window.matchMedia("only screen and (max-width: 62em)").matches;
		$slideWrapp.css("min-height", $slideInner.outerHeight());
		if (!media) {
			$slideWrapp.css("min-height", $slideInner.outerHeight());
		} else {
			$slideWrapp.css("min-height", "initial");
		}
		// recount new slide track
		slideTrackStart = $slideWrapp.offset().top;
		slideTrackEnd = $slideWrapp.offset().top + $slideWrapp.height();
	}

	if ($slideWrapp.length) {
		// wait while fix-menu getting right position
		setTimeout(function () {
			setWrappHeight();
			getMode();
			setSlidePosition();
		}, 50);

		$(window).on("scroll", function () {
			scrollTop = $(window).scrollTop() + fixMenuHeight;
			setWrappHeight();
			getMode();
			setSlidePosition();
		});
		$(window).on("resize", function () {
			scrollTop = $(window).scrollTop() + fixMenuHeight;
			setWrappHeight();
			getMode();
			setSlidePosition();
		});
	}
});//ready