

(function ($) {
	// prived methods
	
	var method_1 = function(value) {
		if ( value === "" ) {
			return true;
		} else {
			return false;
		} 
	};
	// prived methods end
	
	// public methods
	var methods = {
		init: function (options) {

			// default option
			var settings = $.extend({ 
				callback: function() {}
			}, options);
			
			return this.each(function () {
				// variables

				// if plugin was init
				if (!data) {
					// init plugin
					if ( settings.callback ) {
						settings.callback();
					}
				}
			});
		}	
	};
	// public methods end
	$.fn.inmerValidation = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Метод с именем ' + method + ' не существует для jQuery.numberInput');
		}
	};

})(jQuery);

$(function () {

}); //ready