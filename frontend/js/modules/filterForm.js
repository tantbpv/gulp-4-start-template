var FilterForm = function (form) {
	_this = this;
	this.$form = jQuery(form);
	// this.filterQuery = window.location.href.split('#')[1];
	this.disableClass = "is-disable";

	this.init = function () {

		// event listeners

		this.$form.find("input, select").on("change", function () {
			// console.log("test");
			_this.submitTrigger();
		});

		this.$form.on("submit", function (e) {
			e.preventDefault();
			// console.log("test3");
			_this.onSubmit();
		});

		return this.$form;
	};

	this.init();
};

// serialize form
FilterForm.prototype.serializeForm = function () {
	return this.$form.serialize();
};

// disable form while ajax
FilterForm.prototype.disableForm = function () {
	this.$form.addClass(this.disableClass);
};

// enable form after ajax
FilterForm.prototype.enableForm = function () {
	this.$form.removeClass(this.disableClass);
};

// clear form value
FilterForm.prototype.clearForm = function (cb) {
	this.$form[0].reset();

	// this.$form.find("select").trigger("refresh");

	if (typeof cb === "function" && typeof cb !== "undefined") {
		cb(_this.$form);
	}
};

// set url
FilterForm.prototype.setUrl = function (cb) {
	var _query = this.serializeForm();
	// console.log(_query);
	if (_query === "") {
		// window.location.href = "";
		history.pushState('', document.title, window.location.pathname);
	} else {
		window.location.href = "#" + _query;
	}

	if (typeof cb === "function" && typeof cb !== "undefined") {
		cb();
	}
};

// submit form
FilterForm.prototype.submitTrigger = function () {
	this.$form.submit();
};

// submit handler (clear for default)
FilterForm.prototype.onSubmit = function () {
	console.log("submit");
};

// on load check, set form state and submit form if url contain a search query
FilterForm.prototype.onLoad = function () {
	var filterQuery = window.location.href.split('#')[1];

	if (filterQuery === "") return false;

	var _this = this;
	// if url has filter query -> set form state on load
	if (typeof filterQuery !== "undefined" && _this.$form.length) {
		_this.setFormState(filterQuery, function () {
			_this.$form.submit();
		});
		return true;
	}
};

FilterForm.prototype.setFormState = function (query, cb) {
	//clear form before
	this.clearForm(function () {
		console.log(query);
		// set new state form url query
		var query_obj = UrlParser.decodeUrl(query);
		for (prop in query_obj) {

			// if value is not empty
			if (query_obj[prop].length) {
				//find fields with same name
				var $fields = _this.$form.find("[name=" + prop + "]");

				$fields.each(function (cb) {
					// set value for each field which match url query string
					if (jQuery(this).is("select")) {
						var $select = jQuery(this);

						$select.find("option").each(function () {

							if (Array.isArray(query_obj[prop])) {
								// "is array";
								var index = query_obj[prop].indexOf(jQuery(this).val());
								// all taxonomies
								if (index !== -1) {
									jQuery(this).attr("selected", true);
								} else {
									jQuery(this).attr("selected", false);
								}
							} else {
								// "is string";
								$select.val(query_obj[prop]);
							}
							// $select.trigger("refresh");
							$select.selectpicker('refresh');
						});
						if (query_obj[prop].indexOf(jQuery(this).find("option").val()) !== -1) {
							jQuery(this).prop("checked", true);
						}
					}

					if (jQuery(this).is("input:text")) {
						jQuery(this).val(query_obj[prop].toString());
					}

					if (jQuery(this).is("input:checkbox")) {
						if (query_obj[prop].indexOf(jQuery(this).val()) !== -1) {
							jQuery(this).prop("checked", true);
						}
					}
					if (jQuery(this).is("input:radio")) {
						if (query_obj[prop].indexOf(jQuery(this).val()) !== -1) {
							jQuery(this).prop("checked", true);
						}
					}
				});
			}
		}

	});


	if (typeof cb === "function" && typeof cb !== "undefined") {
		cb();
	}
};


