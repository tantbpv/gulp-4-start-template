'use strict';

import DomReady from './modules/DomReady'
import Global from './modules/Global'
import Accordion from'./modules/Accordion'
import Alert from'./modules/Alert'

import './modules/smooth-scroll'
import './modules/Global'
import './modules/XHR'

DomReady(main); // on DOM ready

function main() {
	// set global variables
	window.Global = Global;

	let acc = new Accordion('body', {
		onOpen: function () {
			console.log(this);
		}
	});

	let alert = new Alert([
		"text message or some html",
		"text message or some html 2"
	]);


}
