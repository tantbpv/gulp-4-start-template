# README #

* Front End start template with Gulp 4
* ver. 1.0.0

# Get started #
#### Install node.js [http://nodejs.org](http://nodejs.org)
  
#### Install gulp-cli globally
```$ npm install --global gulp-cli```
  
#### Go to project directory
```$ cd [project_dir]```

#### Install all dependency modules
```$ npm install```

#### Start gulp
```$ gulp```

#### Open http://localhost:3000/ in browser


# BEM and CSS selectors rule #
* ```.block```;
* ```.block_element```;
* ```.block_element--modifier```;
* ```.js-script-name``` - javascript classes;
* ```.is-hidden, .is-active, .is-valid``` - javascript state classes;


# JS variable names rule #

* ```var $variable``` - jQuery object;
* ```var _variable``` - private/local variable;
* ```var Module``` - module name or js Class;

# Code style guide #
[http://sadcitizen.me/code-guide/](http://sadcitizen.me/code-guide/)
[https://github.com/rwaldron/idiomatic.js/tree/master/translations/ru_RU](https://github.com/rwaldron/idiomatic.js/tree/master/translations/ru_RU)



