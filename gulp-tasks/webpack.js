'use strict';

const path = require('path');
const gulplog = require('gulplog');
const webpack = require('webpack');
const notifier = require('node-notifier');


module.exports = function (options) {

	return function (callback) {

		let srcPath  = path.join(__dirname, '/../frontend/js'),
				distPath = path.join(__dirname, '/../public/assets/js');

		let options = {
			entry: {
				main: './main.js',
			},
			output: {
				path: distPath,
				publicPath: '/js/',
				filename: '[name].js',
				// name of the global var: "Global"
				//library: "Global"
			},
			watch: true,
			cache: true,
			devtool: 'cheap-module-inline-source-map',
			context: srcPath,
			module: {
				loaders: [{
					test: /\.js$/,
					include: path.join(__dirname, "/../frontend"),
					loader: 'babel-loader?presets[]=es2015'
				}]
			},
			externals: {
				// require("jquery") is external and available
				//  on the global var jQuery
				"jquery": "jQuery"
			},
			plugins: [
				new webpack.NoEmitOnErrorsPlugin(), // otherwise error still gives a file
				new webpack.optimize.ModuleConcatenationPlugin()
			]
		};

		// https://webpack.github.io/docs/node.js-api.html
		webpack(options, function (err, stats) {
			if (!err) { // no hard error
				// try to get a soft error from stats
				err = stats.toJson().errors[0];
			}

			if (err) {
				notifier.notify({
					title: 'Webpack',
					message: err
				});

				gulplog.error(err);
			} else {
				gulplog.info(stats.toString({
					colors: true
				}));
			}

			// task never errs in watch mode, it waits and recompiles
			if (!options.watch && err) {
				callback(err);
			} else {
				callback();
			}

		});

	}

};
